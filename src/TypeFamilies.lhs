> {-# LANGUAGE FlexibleContexts #-}
> {-# LANGUAGE TypeFamilies #-}
> import Control.Concurrent.STM
> import Control.Concurrent.MVar
> import Data.Foldable (forM_)
> import Data.IORef

> type Present = String
> storePresents :: (Store store, Monad (StoreMonad store))
>               => [Present] -> (StoreMonad store) (store [Present])
> storePresents xs = do
>   store <- new []
>   forM_ xs $ \x -> do
>     old <- get store
>     put store (x : old)
>   return store

You need to give store as an argument to StoreMonad because otherwise the instances complain that StoreMonad was defined multiple times.
I first thought, since StoreMonad is associated with an instance, you should be able to just write StoreMonad if the function takes something of that instance. But of course a function could take two different instances and then it would be ambiguous so type definitions in classes need to be like functions.

> class Store store where
>   type StoreMonad store :: * -> *
>   new :: a -> (StoreMonad store) (store a)
>   get :: store a -> (StoreMonad store) a
>   put :: store a -> a -> (StoreMonad store) ()
> 
> instance Store IORef where
>   type StoreMonad IORef = IO
>   new = newIORef
>   get = readIORef
>   put ioref a = modifyIORef ioref (const a)
> 
> instance Store TVar where
>   type StoreMonad TVar = STM
>   new = newTVar
>   get = readTVar
>   put ioref a = modifyTVar ioref (const a)

