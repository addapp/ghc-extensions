> {-# LANGUAGE FlexibleInstances #-}
> {-# LANGUAGE FlexibleContexts #-}
> {-# LANGUAGE MultiParamTypeClasses #-}
> import Control.Monad.Trans.Class (lift)
> import Control.Monad.Trans.Reader (ReaderT, runReaderT)
> import Data.Foldable (forM_)
> import Data.IORef

Compared to the example using TypeFamilies this is more concise because we don't need to construct the monad using StoreMonad every time.

> class Store store m where
>  new :: a -> m (store a)
>  get :: store a -> m a
>  put :: store a -> a -> m ()
> 
> type Present = String
> storePresents :: (Store store m, Monad m) => [Present] -> m (store [Present])
> storePresents xs = do
>   store <- new []
>   forM_ xs $ \x -> do
>     old <- get store
>     put store (x : old)
>   return store

We can put the IO directly into the type variable of the instance declaration.

> instance Store IORef IO where
>   new = newIORef
>   get = readIORef
>   put ioref a = modifyIORef ioref (const a)


> ex :: (Monad m, Store IORef m) => [Present] -> m [Present]
> ex ps = do
>   store <- storePresents ps
>   get (store :: IORef [Present])

But the inferred type for ex is now less specific since we could also have declared an instance like this.

> instance Store IORef (ReaderT () IO) where
>  new = lift . newIORef
>  get = lift . readIORef
>  put ioref a = lift (modifyIORef ioref (const a))
> x = runReaderT ((ex :: [Present] -> (ReaderT () IO) [Present]) ["SICP"]) () 
