> {-# LANGUAGE FlexibleInstances #-}
> {-# LANGUAGE MultiParamTypeClasses #-}
> {-# LANGUAGE FunctionalDependencies #-}
> import Data.Foldable (forM_)
> import Data.IORef

The functional dependency expressed though the -> tells the compiler that given store it can uniquely determine m

> class Store store m | store -> m where
>   new :: a -> m (store a)
>   get :: store a -> m a
>   put :: store a -> a -> m ()

So that when we write our instance like this, later when the compiler looks for instances it knows this is the only possible one.

> instance Store IORef IO where
>   new = newIORef
>   get = readIORef
>   put ioref a = modifyIORef ioref (const a)

> type Present = String
> storePresents :: (Store store m, Monad m) => [Present] -> m (store [Present])
> storePresents xs = do
>   store <- new []
>   forM_ xs $ \x -> do
>     old <- get store
>     put store (x : old)
>   return store

Now the inferred type of ex is much better. it knows the monad must be IO

> ex :: [Present] -> IO [Present]
> ex ps = do
>   store <- storePresents ps
>   get (store :: IORef [Present])

> x = 1
